package ru.vkandyba.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Boolean existsByIndex(@NotNull String userId, @NotNull Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        return entity.isPresent();
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull String userId, @NotNull String id) {
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        return entity.isPresent();
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull String userId, @NotNull String id) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findByIndex(@NotNull String userId, @NotNull Integer index) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        final String query = "DELETE FROM " + getTableName() +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void removeByIndex(@NotNull String userId, @NotNull Integer index) {
        final String query = "DELETE FROM " + getTableName() +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void add(@NotNull String userId, @NotNull E entity) {
        entity.setUserId(userId);
        add(entity);
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull String userId,@NotNull E entity) {
        final String query = "DELETE FROM " + getTableName() +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull String userId) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        final ResultSet resultSet = statement.executeQuery();
        final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        final ResultSet resultSet = statement.executeQuery();
        final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void clear(String userId) {

    }
}
