package ru.vkandyba.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    protected abstract E fetch(ResultSet resultSet);

    protected abstract String getTableName();

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    @Override
    public Boolean existsByIndex(@NotNull Integer index) {
        final Optional<E> entity = Optional.ofNullable(findByIndex(index));
        return entity.isPresent();
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        return entity.isPresent();
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@NotNull String id) {
        final String query = "SELECT * FROM " + getTableName() + " WHERE " + FieldConst.ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findByIndex(@NotNull Integer index) {
        final String query = "SELECT * FROM " + getTableName() + " WHERE " + FieldConst.INDEX + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Override
    public void removeById(@NotNull String id) {
        final String query = "DELETE FROM " + getTableName() + " WHERE " + FieldConst.ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void removeByIndex(@NotNull Integer index) {
        final String query = "DELETE FROM " + getTableName() + " WHERE " + FieldConst.INDEX + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void add(@NotNull E entity) {
        final String query = "INSERT INTO " + getTableName() + " (" + FieldConst.ID + ") VALUES (?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull E entity) {
        removeById(entity.getId());
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        final String query = "SELECT * FROM " + getTableName();
        final PreparedStatement statement = connection.prepareStatement(query);
        final ResultSet resultSet = statement.executeQuery();
        final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@NotNull Comparator<E> comparator) {
        final String query = "SELECT * FROM " + getTableName();
        final PreparedStatement statement = connection.prepareStatement(query);
        final ResultSet resultSet = statement.executeQuery();
        final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public void addAll(@NotNull List<E> entities) {
        for(E item: entities){
            add(item);
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        final String query = "DELETE FROM " + getTableName();
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

}
