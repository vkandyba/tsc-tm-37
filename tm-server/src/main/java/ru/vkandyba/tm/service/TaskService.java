package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.ITaskService;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.constant.TableConst;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.AbstractBusinessRepository;
import ru.vkandyba.tm.repository.TaskRepository;
import ru.vkandyba.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {


    @Override
    public ITaskRepository getRepository(Connection connection) {
        return new TaskRepository(connection);
    }

    public TaskService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @SneakyThrows
    @Nullable
    @Override
    public Task findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            return repository.findByName(userId, name);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.removeByName(userId, name);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.updateById(userId, id, name, description);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.startById(userId, id);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.startByIndex(userId, index);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.startByName(userId, name);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.finishById(userId, id);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.finishByIndex(userId, index);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.finishByName(userId, name);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.changeStatusById(userId, id, status);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.changeStatusByIndex(userId, index, status);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.changeStatusByName(userId, name, status);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }


    @SneakyThrows
    @Override
    public void create(@Nullable String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new RuntimeException("Error! Description is empty!");
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        final Connection connection = connectionService.getConnection();
        try {
            final ITaskRepository repository = getRepository(connection);
            repository.add(userId, task);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
    
}

