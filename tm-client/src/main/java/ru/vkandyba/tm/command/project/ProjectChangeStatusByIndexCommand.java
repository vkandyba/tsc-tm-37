package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Project;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.Status;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class ProjectChangeStatusByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change status project by index...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter index");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        serviceLocator.getProjectEndpoint().changeProjectStatusByIndex(session, index, status);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }


}
