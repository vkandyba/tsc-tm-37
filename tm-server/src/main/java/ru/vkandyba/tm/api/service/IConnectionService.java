package ru.vkandyba.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

    IPropertyService getPropertyService();
}
