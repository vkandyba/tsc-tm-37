package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    void add(Session session);

}
