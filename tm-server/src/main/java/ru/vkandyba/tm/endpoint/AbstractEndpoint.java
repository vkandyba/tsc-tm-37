package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.service.ServiceLocator;

public abstract class AbstractEndpoint {

    @NotNull
    final ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull ServiceLocator serviceLocator){
        this.serviceLocator = serviceLocator;
    }
}
