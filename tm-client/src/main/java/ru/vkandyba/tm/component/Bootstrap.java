package ru.vkandyba.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.endpoint.*;
import ru.vkandyba.tm.exception.system.UnknownCommandException;
import ru.vkandyba.tm.repository.*;
import ru.vkandyba.tm.service.*;
import ru.vkandyba.tm.util.SystemUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;

@Getter
@Setter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private final Backup backup = new Backup(this);
    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();
    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();
    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();
    @NotNull
    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();
    @Nullable
    private Session session = null;

    @SneakyThrows
    private void initCommands(){
            @NotNull final Reflections reflections = new Reflections("ru.vkandyba.tm.command");
            @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.vkandyba.tm.command.AbstractCommand.class);
            for(Class<? extends AbstractCommand> clazz: classes){
                if(Modifier.isAbstract(clazz.getModifiers())) continue;
                registry(clazz.newInstance());
            }
    }


    @SneakyThrows
    private void initPID(){
        @NotNull final String fileName = "tsc-tm.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes() );
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        logService.debug("Test environment.");
        @Nullable String command = "";
        initCommands();
        initPID();
//        backup.init();
//        fileScanner.init();
        @NotNull final Scanner scanner = new Scanner(System.in);
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND");
                command = scanner.nextLine();
                logService.command(command);
                execute(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
        backup.stop();
    }


    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void execute(@Nullable String command) {
        if (command == null) return;
        final AbstractCommand abstractCommand = getCommandService().getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        abstractCommand.execute();
    }

}
