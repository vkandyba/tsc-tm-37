package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.endpoint.Task;
import ru.vkandyba.tm.enumerated.Role;

import java.util.List;

public class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("[LIST TASKS]");
        List<Task> tasks = serviceLocator.getTaskEndpoint().findAllTasks(session);
        for (Task task : tasks) {
            System.out.println(tasks.indexOf(task) + 1 + ". " + task.getName() + " " + task.getId() + ": " + task.getDescription());
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
