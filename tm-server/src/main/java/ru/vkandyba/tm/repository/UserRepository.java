package ru.vkandyba.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.constant.TableConst;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.empty.EmptyLoginException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @SneakyThrows
    @Override
    protected User fetch(ResultSet resultSet) {
        if(resultSet == null) return null;
        final User user = new User();
        user.setId(resultSet.getString(FieldConst.ID));
        user.setLogin(resultSet.getString(FieldConst.LOGIN));
        user.setEmail(resultSet.getString(FieldConst.EMAIL));
        user.setFirstName(resultSet.getString(FieldConst.FIRST_NAME));
        user.setLastName(resultSet.getString(FieldConst.LAST_NAME));
        user.setMiddleName(resultSet.getString(FieldConst.MIDDLE_NAME));
        user.setLocked(resultSet.getBoolean(FieldConst.LOCKED));
        user.setRole(Role.valueOf(resultSet.getString(FieldConst.ROLE)));
        user.setPasswordHash(resultSet.getString(FieldConst.PASSWORD));
        return user;
    }

    @Override
    protected String getTableName() {
        return TableConst.USER_TABLE;
    }

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        final String query = "SELECT * FROM " + getTableName() + " WHERE " + FieldConst.LOGIN + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final User result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Override
    public void removeUser(@NotNull User user) {
        final String query = "DELETE FROM " + getTableName() + " WHERE " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void removeByLogin(@NotNull String login) {
        final String query = "DELETE FROM " + getTableName() + " WHERE " + FieldConst.LOGIN + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void updateUser(String userId, String firstName, String lastName, String middleName) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.FIRST_NAME + "=?, " + FieldConst.LAST_NAME + "='?, " + FieldConst.MIDDLE_NAME + "=?" +
                " WHERE " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, firstName);
        statement.setString(2, lastName);
        statement.setString(3, middleName);
        statement.setString(4, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(String login) {
        final String query = "UPDATE " + getTableName() + " SET " + FieldConst.LOCKED + "=?" +
                " WHERE " + FieldConst.LOGIN + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setBoolean(1, true);
        statement.setString(2, login);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(String login) {
        final String query = "UPDATE " + getTableName() + " SET " + FieldConst.LOCKED + "=?" +
                " WHERE " + FieldConst.LOGIN + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setBoolean(1, false);
        statement.setString(2, login);
        statement.executeUpdate();
        statement.close();
    }

}
