package ru.vkandyba.tm.exception.empty;

import ru.vkandyba.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty!");
    }

}
