package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.repository.ProjectRepository;

import java.sql.Connection;

public class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    @Override
    public IProjectRepository getRepository(Connection connection) {
        return new ProjectRepository(connection);
    }

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @SneakyThrows
    @Nullable
    @Override
    public Project findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            return repository.findByName(userId, name);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.removeByName(userId, name);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.updateById(userId, id, name, description);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.startById(userId, id);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.startByIndex(userId, index);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.startByName(userId, name);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.finishById(userId, id);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.finishByIndex(userId, index);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.finishByName(userId, name);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.changeStatusById(userId, id, status);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.changeStatusByIndex(userId, index, status);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new RuntimeException("Error! Status is empty!");
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.changeStatusByName(userId, name, status);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.add(userId, project);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new RuntimeException("Error! Description is empty!");
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        final Connection connection = connectionService.getConnection();
        try {
            final IProjectRepository repository = getRepository(connection);
            repository.add(userId, project);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
