package ru.vkandyba.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.constant.TableConst;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @SneakyThrows
    @Override
    protected Task fetch(ResultSet resultSet) {
        @NotNull final Task task = new Task();
        task.setName(resultSet.getString(FieldConst.NAME));
        task.setId(resultSet.getString(FieldConst.ID));
        task.setUserId(resultSet.getString(FieldConst.USER_ID));
        task.setDescription(resultSet.getString(FieldConst.USER_ID));
        task.setCreatedDate(resultSet.getDate(FieldConst.CREATED_DATE));
        task.setFinishDate(resultSet.getDate(FieldConst.FINISH_DATE));
        task.setStartDate(resultSet.getDate(FieldConst.START_DATE));
        task.setStatus(Status.valueOf(resultSet.getString(FieldConst.STATUS)));
        task.setProjectId(resultSet.getString(FieldConst.PROJECT_ID));
        return task;
    }

    @Override
    protected String getTableName() {
        return TableConst.TASK_TABLE;
    }

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @SneakyThrows
    @Override
    public void add(@NotNull Task task){
        final String query = "INSERT INTO " + getTableName() +
                " (" + FieldConst.ID + ", " + FieldConst.USER_ID + ", " + FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " + FieldConst.STATUS + ", " + FieldConst.START_DATE + ", " +
                FieldConst.FINISH_DATE + ", " + FieldConst.CREATED_DATE + ", " + FieldConst.PROJECT_ID + ") " +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getUserId());
        statement.setString(3, task.getName());
        statement.setString(4, task.getDescription());
        statement.setString(5, task.getStatus().toString());
        statement.setDate(6, task.getStartDate() != null ? new Date( task.getStartDate().getTime()) : null);
        statement.setDate(7, task.getFinishDate() != null ? new Date( task.getFinishDate().getTime()) : null);
        statement.setDate(8, task.getCreatedDate() != null ? new Date( task.getCreatedDate().getTime()) : null);
        statement.setString(9, task.getProjectId());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void updateById(String userId, String id, String name, String description) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.NAME + "='" + name + "', " +
                FieldConst.DESCRIPTION + "='" + description + "'" +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, id);
        statement.setString(4, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Nullable
    @Override
    public Task findByName(@NotNull String userId, @NotNull String name) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Override
    public void removeByName(@NotNull String userId, @NotNull String name) {
        final String query = "DELETE FROM " + getTableName() +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void startById(@NotNull String userId, @NotNull String id) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.IN_PROGRESS +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void startByIndex(@NotNull String userId, @NotNull Integer index) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.IN_PROGRESS +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void startByName(@NotNull String userId, @NotNull String name) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.IN_PROGRESS +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void finishById(@NotNull String userId, @NotNull String id) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.COMPLETED +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.COMPLETED +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void finishByName(@NotNull String userId, @NotNull String name) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.COMPLETED +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + status +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + status +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + status +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.PROJECT_ID + "=" + projectId +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, taskId);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void unbindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.PROJECT_ID + "= NULL" +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, taskId);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.USER_ID + "=? AND " + FieldConst.PROJECT_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        final ResultSet resultSet = statement.executeQuery();
        final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @SneakyThrows
    @Override
    public void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) {
        final String query = "DELETE FROM " + getTableName() +
                " WHERE " + FieldConst.PROJECT_ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

}
