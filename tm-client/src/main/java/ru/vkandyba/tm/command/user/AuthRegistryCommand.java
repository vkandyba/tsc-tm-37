package ru.vkandyba.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.util.TerminalUtil;

public class AuthRegistryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Registry user...";
    }

    @Override
    public void execute() {
        System.out.println("Enter Login");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("Enter email");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().createUser(login, password, email);
    }

}
