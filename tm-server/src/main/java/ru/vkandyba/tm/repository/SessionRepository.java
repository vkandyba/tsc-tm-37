package ru.vkandyba.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.repository.ISessionRepository;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.constant.TableConst;
import ru.vkandyba.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @SneakyThrows
    @Override
    protected Session fetch(ResultSet resultSet) {
        final Session session = new Session();
        session.setId(resultSet.getString(FieldConst.ID));
        session.setUserId(resultSet.getString(FieldConst.USER_ID));
        session.setSignature(resultSet.getString(FieldConst.SIGNATURE));
        session.setTimestamp(resultSet.getLong(FieldConst.TIMESTAMP));
        return session;
    }

    @Override
    protected String getTableName() {
        return TableConst.SESSION_TABLE;
    }

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @SneakyThrows
    @Override
    public void add(@NotNull Session session) {
        final String query =
                "INSERT INTO app_sessions (id, user_id, signature, timestamp) " +
                        "VALUES(?, ?, ?, ?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setString(4, session.getTimestamp().toString());
        statement.execute();
        statement.close();
    }

}
