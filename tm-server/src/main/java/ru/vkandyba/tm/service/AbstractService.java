package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractEntity;
import ru.vkandyba.tm.repository.AbstractRepository;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    IConnectionService connectionService;

    public abstract IRepository<E> getRepository(Connection connection);

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @SneakyThrows
    @NotNull
    @Override
    public Boolean existsByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.existsByIndex(index);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.existsById(id);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.findByIndex(index);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.removeById(id);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.removeByIndex(index);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable E entity) {
        if (entity == null) throw new RuntimeException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.add(entity);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable E entity) {
        if (entity == null) throw new RuntimeException();
        removeById(entity.getId());
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll() {
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable Comparator<E> comparator) {
        if(comparator == null) throw new RuntimeException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if(entities == null) throw new RuntimeException();
        for(E item: entities){
            add(item);
        }
    }

    @SneakyThrows
    @Override
    public void clear() {
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
