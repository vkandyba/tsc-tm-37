package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    void bindTaskToProjectById(String userId, String projectId, String taskId);

    void unbindTaskToProjectById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void removeById(String userId, String projectId);

}
