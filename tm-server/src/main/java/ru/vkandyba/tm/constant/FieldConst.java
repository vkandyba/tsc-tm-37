package ru.vkandyba.tm.constant;

import ru.vkandyba.tm.enumerated.Status;

import java.util.Date;

public class FieldConst {

    public static String ID = "id";

    public static String USER_ID = "user_id";
    
    public static String NAME = "name";

    public static String DESCRIPTION = "description";

    public static String STATUS = "status";

    public static String START_DATE = "start_date";

    public static String FINISH_DATE = "finish_date";

    public static String CREATED_DATE = "created_date";

    public static String INDEX = "index";

    public static String PROJECT_ID = "project_id";

    public static String SIGNATURE = "signature";

    public static String TIMESTAMP = "timestamp";

    public static String LOGIN = "login";

    public static String EMAIL = "email";

    public static String FIRST_NAME = "first_name";

    public static String LAST_NAME = "last_name";

    public static String MIDDLE_NAME = "middle_name";

    public static String LOCKED = "locked";

    public static String ROLE = "role";

    public static String PASSWORD = "password";

}
