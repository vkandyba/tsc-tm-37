package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.api.repository.IBusinessRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessRepository<E> {

    public abstract IBusinessRepository<E> getRepository(Connection connection);

    public AbstractBusinessService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }
    
    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (comparator == null) throw new RuntimeException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Boolean existsByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            return repository.existsByIndex(userId, index);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            return repository.existsById(userId, id);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findById(userId, id);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Nullable
    @Override
    public E findByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findByIndex(userId,index);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            repository.removeById(userId, id);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            repository.removeByIndex(userId, index);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void add(@Nullable String userId, @Nullable E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (entity == null) throw new RuntimeException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            entity.setUserId(userId);
            repository.add(entity);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
    
    @Override
    public void remove(@Nullable String userId, @Nullable E entity) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (entity == null) throw new RuntimeException();
        removeById(entity.getId());
    }

    @SneakyThrows
    @Override
    public void clear(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final Connection connection = connectionService.getConnection();
        try {
            final IBusinessRepository<E> repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
