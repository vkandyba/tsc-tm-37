package ru.vkandyba.tm.api.service;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    IAuthService getAuthService();

    IUserService getUserService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

}
