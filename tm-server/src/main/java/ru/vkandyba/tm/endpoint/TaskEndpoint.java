package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.ITaskEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.Session;

import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<Task> findAllTasks(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    public void addTask(@NotNull Session session, @NotNull Task task) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(session.getUserId(),task);
    }

    @Override
    public void changeTaskStatusById(@NotNull Session session, @NotNull String taskId, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusById(session.getUserId(), taskId, status);
    }

    @Override
    public void changeTaskStatusByIndex(@NotNull Session session, @NotNull Integer index, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    public void changeTaskStatusByName(@NotNull Session session, @NotNull String name, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    public void finishTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishById(session.getUserId(), taskId);
    }

    @Override
    public void finishTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @Override
    public void finishTaskByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @Override
    public void removeTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeById(session.getUserId(), taskId);
    }

    @Override
    public void removeTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    public void removeTaskByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    public Task showTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), taskId);
    }

    @Override
    public Task showTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    public void startTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startById(session.getUserId(), taskId);
    }

    @Override
    public void startTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @Override
    public void startTaskByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @Override
    public void updateTaskById(@NotNull Session session, @NotNull String taskId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateById(session.getUserId(), taskId, name, description);
    }

    @Override
    public void removeTaskWithTasksById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeById(session.getUserId(), taskId);
    }

    @Override
    public void clearTasks(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    public void unbindTaskFromProjectById(@NotNull Session session, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().unbindTaskToProjectById(session.getUserId(), projectId, taskId);
    }

    @Override
    public @Nullable List<Task> showAllTasksByProjectId(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    public void bindTaskToProjectById(@NotNull Session session, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().bindTaskToProjectById(session.getUserId(), projectId, taskId);
    }

}
