package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IBusinessRepository<Task> {

    Task findByName(String userId, String name);

    void removeByName(String userId, String name);

    void startById(String userId, String id);

    void startByIndex(String userId, Integer index);

    void startByName(String userId, String name);

    void finishById(String userId, String id);

    void finishByIndex(String userId, Integer index);

    void finishByName(String userId, String name);

    void changeStatusById(String userId, String id, Status status);

    void changeStatusByIndex(String userId, Integer index, Status status);

    void changeStatusByName(String userId, String name, Status status);

    void bindTaskToProjectById(String userId, String projectId, String taskId);

    void unbindTaskToProjectById(String userId, String projectId, String taskId);

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void updateById(String userId, String id, String name, String description);

}
