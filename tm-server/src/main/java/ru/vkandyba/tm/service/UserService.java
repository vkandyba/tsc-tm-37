package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.api.service.IConnectionService;
import ru.vkandyba.tm.api.service.IPropertyService;
import ru.vkandyba.tm.api.service.IUserService;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.empty.*;
import ru.vkandyba.tm.exception.entity.UserNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.repository.UserRepository;
import ru.vkandyba.tm.util.HashUtil;

import java.sql.Connection;
import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private IPropertyService propertyService;

    public UserService(@NotNull IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    public IUserRepository getRepository(Connection connection) {
        return new UserRepository(connection);
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeUser(@Nullable User user) {
        if (user == null) throw new EmptyLoginException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            repository.removeUser(user);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            repository.removeByLogin(login);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String login, @Nullable String password, @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        if(email == null || email.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setEmail(email);
            repository.add(user);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String login, @Nullable String password, @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        if(role == null) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            repository.add(user);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void create(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if(password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            final User user = new User();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.add(user);
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void updateUser(@Nullable String userId, @Nullable String firstName,
                           @Nullable String lastName, @Nullable String middleName)
    {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyIdException();
        if(lastName == null || lastName.isEmpty()) throw new EmptyNameException();
        if(middleName == null || middleName.isEmpty()) throw new EmptyNameException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            repository.updateUser(userId, firstName, lastName, middleName);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            repository.lockUserByLogin(login);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final Connection connection = connectionService.getConnection();
        try {
            final IUserRepository repository = getRepository(connection);
            repository.unlockUserByLogin(login);
            connection.commit();
        } catch (Exception e){
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
