package ru.vkandyba.tm.repository;

import lombok.SneakyThrows;
import org.eclipse.persistence.annotations.Mutable;
import org.eclipse.persistence.annotations.ObjectTypeConverter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.constant.FieldConst;
import ru.vkandyba.tm.constant.TableConst;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @SneakyThrows
    @Override
    protected Project fetch(ResultSet resultSet) {
        if(resultSet == null) return null;
        @NotNull final Project project = new Project();
        project.setName(resultSet.getString(FieldConst.NAME));
        project.setId(resultSet.getString(FieldConst.ID));
        project.setUserId(resultSet.getString(FieldConst.USER_ID));
        project.setDescription(resultSet.getString(FieldConst.USER_ID));
        project.setCreatedDate(resultSet.getDate(FieldConst.CREATED_DATE));
        project.setFinishDate(resultSet.getDate(FieldConst.FINISH_DATE));
        project.setStartDate(resultSet.getDate(FieldConst.START_DATE));
        project.setStatus(Status.valueOf(resultSet.getString(FieldConst.STATUS)));
        return project;
    }

    @Override
    protected String getTableName() {
        return TableConst.PROJECT_TABLE;
    }

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @SneakyThrows
    @Override
    public void add(@NotNull Project project){
        final String query = "INSERT INTO " + getTableName() +
                " (" + FieldConst.ID + ", " + FieldConst.USER_ID + ", " + FieldConst.NAME + ", " +
                FieldConst.DESCRIPTION + ", " + FieldConst.STATUS + ", " + FieldConst.START_DATE + ", " +
                FieldConst.FINISH_DATE + ", " + FieldConst.CREATED_DATE + ") " +
                "VALUES (?,?,?,?,?,?,?,?)";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDescription());
        statement.setString(5, project.getStatus().toString());
        statement.setDate(6, project.getStartDate() != null ? new Date( project.getStartDate().getTime()) : null);
        statement.setDate(7, project.getFinishDate() != null ? new Date( project.getFinishDate().getTime()) : null);
        statement.setDate(8, project.getCreatedDate() != null ? new Date( project.getCreatedDate().getTime()) : null);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void updateById(String userId, String id, String name, String description) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.NAME + "='" + name + "', " +
                FieldConst.DESCRIPTION + "='" + description + "'" +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, id);
        statement.setString(4, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Nullable
    @Override
    public Project findByName(@NotNull String userId, @NotNull String name) {
        final String query = "SELECT * FROM " + getTableName() +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        final ResultSet resultSet = statement.executeQuery();
        if(!resultSet.next()) return null;
        final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @SneakyThrows
    @Override
    public void removeByName(@NotNull String userId, @NotNull String name) {
        final String query = "DELETE FROM " + getTableName() +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void startById(@NotNull String userId, @NotNull String id) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.IN_PROGRESS +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void startByIndex(@NotNull String userId, @NotNull Integer index) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.IN_PROGRESS +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void startByName(@NotNull String userId, @NotNull String name) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.IN_PROGRESS +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void finishById(@NotNull String userId, @NotNull String id) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.COMPLETED +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.COMPLETED +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void finishByName(@NotNull String userId, @NotNull String name) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + Status.COMPLETED +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + status +
                " WHERE " + FieldConst.ID + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + status +
                " WHERE " + FieldConst.INDEX + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final String query = "UPDATE " + getTableName() +
                " SET " + FieldConst.STATUS + "=" + status +
                " WHERE " + FieldConst.NAME + "=? AND " + FieldConst.USER_ID + "=?";
        final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

}
