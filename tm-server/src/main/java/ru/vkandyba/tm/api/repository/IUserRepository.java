package ru.vkandyba.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    void removeUser(User user);

    void removeByLogin(String login);

    void updateUser(String userId, String firstName, String lastName, String middleName);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
